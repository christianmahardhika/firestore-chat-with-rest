### Message
#### Send Message
```
curl --location --request POST 'http://localhost:3000/message/sendMessage' \
--header 'Content-Type: application/json' \
--data-raw '{
    "message": "Hello"
}'
```   

### User
#### Get All User
```
curl --location --request GET 'http://localhost:3000/user/getAllUsers'
```  