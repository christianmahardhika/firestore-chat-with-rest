require('dotenv').config()

const express = require('express');
const app = express();
const router = express.Router();

const bodyParser = require('body-parser')
app.use(bodyParser.json())

const init = require('./config/init/index')

app.set('json spaces', 2)

const appConfig = {
  name: "FireStoreBali"
}
init.initApplication(appConfig)

const userAPI = require('./src/api/user')

app.get('/user/getAllUsers', userAPI.getAllUsers);



app.get('/room/getAllRoom', (req,res) => {
  const room = require('./src/client/firebase/room')
  const foundRooms = room.getAllRoom()

  const response = {
    code: 201,
    entity: "message",
    message: "Success Send Message",
    data: foundRooms
  }

  res.status(response.code).header("Content-Type",'application/json').json(response);
});

app.post('/message/sendMessage', (req,res) => {
  const { message } = req.body
  console.log(message)

  const response = {
    code: 201,
    entity: "message",
    message: "Success Send Message",
  }

  res.status(response.code).header("Content-Type",'application/json').json(response);
});

app.use('/', router);

app.listen(process.env.APP_PORT || 3000);

console.log(`Web Server is listening at port ${(process.env.APP_PORT || 3000)} with name : ${process.env.APP_NAME}`);