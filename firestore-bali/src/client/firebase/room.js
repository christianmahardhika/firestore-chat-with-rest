var fire = require('../../../config/firebase')
var db = fire.firestore()
db.settings({
    timestampsInSnapshots: true
})

module.exports = {
    createOneRoom: async function(input) {
        db.collection('room').add({
            room: input.name,
        })
    },
    getAllRooms : async function() {
        const doc = await db.collection(`room`).get()
          const data = doc.data()
          if (!data) {
            console.error('room does not exist')
            return
          }
          return data
    }
}

