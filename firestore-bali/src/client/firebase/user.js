var fire = require('../../../config/firebase')
var db = fire.firestore()
db.settings({
    timestampsInSnapshots: true
})

module.exports = {
    createOneUser: async function(input) {
        db.collection('user').add({
            name: input.name,
        })
    },
    getOneUser: async function(input) {
        const doc = await db.collection(`user`).where('name', '==', input.name).get();
        const users = []
        doc.forEach(d => {
            users.push(d.data())
        });
        return users[0]
    },
    getAllUsers : async function() {
        const doc = await db.collection(`user`).get();
        const users = []
        doc.forEach(d => {
            users.push(d.data())
        });
        return users
    }
}

