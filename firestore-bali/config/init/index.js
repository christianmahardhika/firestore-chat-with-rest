module.exports = {
    initApplication: async function(input) {
        const user = require('../../src/client/firebase/user')
        const foundUser = await user.getOneUser(input)
        if (!foundUser) {
            user.createOneUser(input)
        }
    }
}

