module.exports = {
    getAllUsers: async function(req,res) {
        const user = require('../client/firebase/user')
        const foundUsers = await user.getAllUsers(req.firebase)
      
        const response = {
          code: 201,
          entity: "message",
          message: "Success Send Message",
          data: foundUsers
        }
      
        res.status(response.code).header("Content-Type",'application/json').json(response);
      }
}