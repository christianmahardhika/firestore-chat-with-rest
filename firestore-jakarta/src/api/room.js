module.exports = {
    createOneRoom: async function(req,res) {
        const { user } = req.body
        const roomData = {
            name: `${user} x ${req.appConfig.name}`
        }
        const room = require('../client/firebase/room')

        await room.createOneRoom(req.firebase, roomData)
      
        const response = {
          code: 201,
          entity: "message",
          message: "Success Create Room"
        }
      
        res.status(response.code).header("Content-Type",'application/json').json(response);
      }
}