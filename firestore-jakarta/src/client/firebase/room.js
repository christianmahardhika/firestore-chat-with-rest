module.exports = {
    createOneRoom: async function(db, input) {
        await db.collection('room').add({
            room: input.name,
        })
        return
    },
    getAllRooms : async function(db) {
        const doc = await db.collection(`room`).get()
          const data = doc.data()
          if (!data) {
            console.error('room does not exist')
            return
          }
          return data
    }
}

