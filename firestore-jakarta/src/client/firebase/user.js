module.exports = {
    createOneUser: async function(db, input) {
        await db.collection('user').add({
            name: input.name,
        })
        return
    },
    getOneUser: async function(db, input) {
        const doc = await db.collection(`user`).where('name', '==', input.name).get();
        const users = []
        doc.forEach(d => {
            users.push(d.data())
        });
        return users[0]
    },
    getAllUsers : async function(db) {
        const doc = await db.collection(`user`).get();
        const users = []
        doc.forEach(d => {
            users.push(d.data())
        });
        return users
    }
}

