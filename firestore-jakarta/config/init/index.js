module.exports = {
    initApplication: async function(db, input) {
        const user = require('../../src/client/firebase/user')
        const foundUser = await user.getOneUser(db, input)
        if (!foundUser) {
            user.createOneUser(input)
        }
        return
    }
}

